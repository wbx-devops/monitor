# My image
FROM arthurcrawford/monitor-base:1.0.0
MAINTAINER Art <arthur.crawford@webexpenses.com>

COPY conf.d /etc/icinga2/conf.d
COPY wbx_check_node /usr/lib64/nagios/plugins/
RUN chown -R icinga:icinga /etc/icinga2/conf.d; \
    chmod 640 /etc/icinga2/conf.d/*; \
    chmod +x /usr/lib64/nagios/plugins/wbx_check_node
