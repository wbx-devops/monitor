monitor
=======

Docker image for webexpenses network monitoring server.

It inherits from the Docker image `arthurcrawford/monitor-base` which is an implementation of 
the Icinga2 monitoring stack, web console and mysql back-end.

To the base image, this image adds the configuration suitable for the webexpenses system architecture.


