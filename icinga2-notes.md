
The Docker builds
-----------------

Most of the `Docker` stuff is in the following repository:

    arthurcrawford/monitor-base
    
The code for this base monitor image is in the git public repository here. 

    https://github.com/arthurcrawford/monitor-base.git

The *internal* webexpenses monitor image is private and is hosted on our private *BitBucket* repository here:

    git@bitbucket.org:wbx-devops/monitor.git

This *Docker* image uses the public monitor-base as its base image.


Starting an Icinga2 Docker Container
----------------------------

The Docker container runs all that is needed for your `Icinga2` master server.  The following sections show you how to run the Docker container and then how to configure the node as a master node.

    $ docker run -ti -p 9393:80 -p 5665:5665 arthur/icinga:v21 bash
    
Run the root `supervisord` process - this coordinates the various processes inside the container (such as `mysql`, `apache` and `Icinga2` itself).

    $ supervisord &

Browse to the URL.

    http://192.168.59.103:9393/icinga-web   (this is if using Boot2docker)

or if you are using a Linux Docker host simply

    http://hostname:9393/icinga-web        (this is if on Linux Docker host)
    
Login using the username / password (root/password by default)

    username: root
    password: password

The `Docker` run command maps the port 80 used by the container's apache instance.  It also exposes port 5665 which is the default port used by `Icinga2` to communicate between master and slaves.

On Master:

	[root@884efc710785 /]# history
	    1  supervisord &					<< Run all process on the docker image
	    2  icinga2 node wizard              << Run icinga2 wizard to setup as master
	    3  supervisorctl restart icinga     << Restart icinga
	    8  icinga2 pki ticket --cn DEVOPS
	    9  icinga2 node update-config
	   11  supervisorctl restart icinga
	   12  icinga2 node update-config
	   13  supervisorctl restart icinga
	   14  history

On non-master (client)
Install icinga2

Run node wizard on client

	C:\Program Files (x86)\ICINGA2\sbin>icinga2.exe node wizard
	Welcome to the Icinga 2 Setup Wizard!

	We'll guide you through all required configuration details.



	Please specify if this is a satellite setup ('n' installs a master setup) [Y/n]:
	 Y
	Starting the Node setup routine...
	Please specifiy the common name (CN) [DEVOPS]:
	Please specifiy the local zone name [DEVOPS]:
	Please specify the master endpoint(s) this node should connect to:
	Master Common Name (CN from your master setup): 884efc710785
	Please fill out the master connection information:
	Master endpoint host (optional, your master's IP address or FQDN): 192.168.1.182

	Master endpoint port (optional) []:
	Add more master endpoints? [y/N]N
	Please specify the master connection for CSR auto-signing (defaults to master en
	dpoint host):
	Host [192.168.1.182]:
	Port [5665]:

To run the daemon (should be a service really)

	C:\Program Files (x86)\ICINGA2\sbin>icinga2.exe daemon

Browse on master

	http://192.168.59.103:9393/icinga-web/
	root
	password

Make sure docker port is exposed:

```bash
boot2docker ssh -vnNTL 192.168.1.182:5665:localhost:5665
```

icinga2 config
==============

Have a look in the following directory.

```bash
/etc/icinga2/conf.d
```

For example, add an HTTP service for a host as follows in the file `services.conf`

```bash
object Service "http" {
  host_name = "login.webexpenses.com"
  check_command = "http"
  check_interval = 30s
}
```

Some additional icinga2 setup gotchas
=======================

For the icinga-web install (icinga-web/v1.11.2) on CentOS 6, the following file must be edited to correct the location of the command pipe file.

```bash
/usr/share/icinga-web/app/modules/Api/config/access.xml 
```
The following fragment has to be updated to refer to the correct file.

```xml
<!-- allowed to be written to -->
  <write>
    <files>
      <resource name="icinga_pipe">/var/run/icinga2/cmd/icinga2.cmd</resource>
    </files>
  </write>
```

Also there may be a permisssions issue for the file.

```bash
# ls -l /var/run/icinga2/cmd/icinga2.cmd
prw-rw---- 1 icinga icingacmd 0 Sep  8 12:52 /var/run/icinga2/cmd/icinga2.cmd
```

The web user -  `apache` in my case has to be added to the icingacmd group.

```bash
usermod -a -G icingacmd apache
```

For icincaweb2:

```
usermod -a -G icingaweb2 apache
```

I *think* its also necessary to modify this file.

```
vi /etc/icinga2/features-enabled/ido-mysql.conf
```

IDO is the **core** database.  The Dockerfile builds a mysql DB called icinga with username:password of icinga.

```bash
library "db_ido_mysql"

object IdoMysqlConnection "ido-mysql" {
  user = "icinga"
  password = "icinga"
  host = "localhost"
  database = "icinga"
}
```

date.timezone' is not defined in `/etc/php.ini`

```bash
date.timezone = Europe/London
```

The following host definition can be added to the file `/etc/icinga2/conf.d/hosts.conf`.




```bash
object Host "login.webexpenses.com" {
  check_command = "tcp"
  vars.tcp_address = "178.238.131.26"
  vars.tcp_port = "80"

  vars.http_address = "178.238.131.26"

  vars.http_vhosts["login.webexpenses.com"] = {
    http_address = "178.238.131.26"
    http_vhost = "login.webexpenses.com"
  }


  vars.notification["mail"] = {
    groups = [ "icingaadmins" ]
  }
}
```

